if('serviceWorker' in navigator){
    send().catch(err => console.log(err))
}

function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
      .replace(/-/g, '+')
      .replace(/_/g, '/');
   
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
   
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }
   
const vapidPublicKey = 'BDZbfS7HRGxyuQzzY6BN8Ase2l3RMx2F-v7uxW2wmfikRKJzTxX8QQ_qvxGYTHJpmPZZ84ujmcNUWnO6Yl5w96g';
const convertedVapidKey = urlBase64ToUint8Array(vapidPublicKey);
   
async function send() {
    console.log('Registrando el service worker');
    const register = await navigator.serviceWorker.register('/worker.js',{
        scope: '/'
    })
    console.log('Service worker registrado');

    console.log('Registrando push');
    const subscription = await register.pushManager.subscribe({
        userVisibleOnly: true,
        applicationServerKey: convertedVapidKey
    });
    console.log('Push registrado');

    console.log('enviando notification push');
    await fetch('/subscribe',{
        method: 'POST',
        body: JSON.stringify(subscription),
        headers: {
            'content-type': 'application/json'
        }
    });
    console.log('Push enviado');
}