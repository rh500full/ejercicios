const express = require('express'),
http = require('http'),
app = express(),
server = http.createServer(app),
io = require('socket.io').listen(server);

app.get('/',(req,res)=>{
    res.send('Corriendo en 3000')
})

io.on('connection',(socket)=>{
    console.log("usuaro conectado");
    socket.on("join",(userNickname)=>{
        console.log(userNickname+" se ha conectado al chat");
        console.log("enviando userjoinedchat");
        socket.broadcast.emit("userjoinedchat",userNickname+" se ha conectado al chat")
    })

    socket.on('messagedetection',(senderNickname,messageContent)=>{
        console.log(senderNickname+" :"+messageContent);

        let message = {"message": messageContent,"senderNickname": senderNickname}
        socket.emit("message",message)
    })

    socket.on('disconnect',()=>{
        console.log('El usuario se fue');
        socket.broadcast.emit('userdisconnect','El usuario se fue')
    })
})



server.listen(3000,()=>{
    console.log('App corriendo en 3000');
})