const electron = require('electron');
const ipc = electron.ipcRenderer;

const syncBtn = document.getElementById('syncBtn');
const asyncBtn = document.getElementById('asyncBtn');

syncBtn.addEventListener('click',()=>{
    console.log('sync msg 1');
    const reply = ipc.sendSync('sync-message')
    console.log(reply);
    console.log('sync msg 2');
})

asyncBtn.addEventListener('click',()=>{
    console.log('async msg 1');
    ipc.send('async-message');
    console.log('async msg 2');
})

ipc.on('opened-error-dialog',(event,arg)=>{
    console.log(arg);
})

ipc.on('async-reply',(event,arg)=>{
    console.log(arg);
})

const BrowserWindow = electron.remote.BrowserWindow;
let window = new BrowserWindow();
window.loadURL('https://github.com/');