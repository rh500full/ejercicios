const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const path = require('path');
const url = require('url');
const ipc = electron.ipcMain;
const dialog = electron.dialog;

let win;

function createWindow() {
    win = new BrowserWindow();
    win.loadURL(url.format({
        pathname: path.join(__dirname,'index.html'),
        protocol: 'file',
        slashes: true
    }));


    ipc.on('sync-message',(event)=>{
        // dialog.showErrorBox('Un error','Esto es un evento de error!')
        event.returnValue = 'sync-reply';
    })

    ipc.on('async-message',(event)=>{
        // dialog.showErrorBox('Un error','Esto es un evento de error!')
        event.sender.send('async-reply','Main process opened async!');
    })

    win.on('closed',()=>{
        win = null;
    })
}

app.on('ready',createWindow);