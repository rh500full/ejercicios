const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const path = require('path');
const url = require('url');
const Menu = electron.Menu;
const MenuItem = electron.MenuItem;
const globalShorcut = electron.globalShortcut;

let win;

function createWindow() {
    win = new BrowserWindow();
    win.loadURL(url.format({
        pathname: path.join(__dirname,'index.html'),
        protocol: 'file',
        slashes: true
    }));
    win.on('closed',()=>{
        win = null;
    })
}

app.on('will-quit',()=>{
    globalShorcut.unregisterAll();
})

app.on('ready',function () {
    createWindow();
    const template = [
        {
            label: 'Edit',
            submenu: [
                { role: 'undo'},
                { role: 'redo'},
                { type: 'separator'},
                { role: 'cut'},
                { role: 'copy'},
                { role: 'paste'},
                { role: 'pasteandmatchstyle'},
                { role: 'delete'},
                { role: 'selectall'},
            ]
        },
        {
            label: 'demo',
            submenu: [
                {
                    label: 'submenu1',
                    click: function () {
                        console.log("Clicked el primero");
                    }
                },
                {
                    type: 'separator'
                },
                {
                    label: 'submenu2'
                }
            ]
        },
        {
            label: 'Help',
            submenu: [
                {
                    label: 'About electron',
                    click: function () {
                        electron.shell.openExternal('http://electron.atom.io');
                    },
                    accelerator: 'CmdOrCtrl + H'
                }
            ]
        }
    ]
    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);

    const ctxMenu = new Menu();
    ctxMenu.append(new MenuItem({
        label: 'Hello',
        click: function () {
            console.log('context-menu');
        }
    }))
    ctxMenu.append(new MenuItem({role: 'selectall'}))

    win.webContents.on('context-menu',(e,params)=>{
        ctxMenu.popup(win,params.x,params.y);
    })

    globalShorcut.register('Alt+1',()=>{
        win.show();
    })
});