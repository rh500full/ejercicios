const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const path = require('path');
const url = require('url');
const Tray = electron.Tray;
const iconPath = path.join(__dirname,'untitled.png');
const Menu = electron.Menu;
let win;
let tray = null;

function createWindow() {
    win = new BrowserWindow();
    win.loadURL(url.format({
        pathname: path.join(__dirname,'index.html'),
        protocol: 'file',
        slashes: true
    }));

    win.on('closed',()=>{
        win = null;
    })
}

app.on('ready',function () {
    tray = new Tray(iconPath);

    let template = [
        {
            label: 'Low',
            type: 'radio',
            checked: true
        },
        {
            label: 'High',
            type: 'radio'
        },
        {
            label: 'Video',
            submenu: [
                {
                    label: '1280x720',
                    type: 'radio',
                    checked: true
                },
                {
                    label: '1920x1080',
                    type: 'radio'
                }
            ]
        }
    ];

    const ctxMenu = Menu.buildFromTemplate(template);
    tray.setContextMenu(ctxMenu);
    tray.setToolTip('Aplication')
});