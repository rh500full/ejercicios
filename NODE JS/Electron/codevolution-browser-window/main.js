const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const path = require('path');
const url = require('url');

let win, dimWindow,colorWindow,framelessWindow;
let parentWindow,childWindow;
function createWindow() {
    // win = new BrowserWindow();
    // dimWindow = new BrowserWindow({width:400,height:400,maxWidth: 600, maxHeight: 600});
    // colorWindow = new BrowserWindow({backgroundColor: '#22Bb22'})
    // framelessWindow = new BrowserWindow({backgroundColor: '#800000', frame:false});
    // win.loadURL(url.format({
    //     pathname: path.join(__dirname,'index.html'),
    //     protocol: 'file',
    //     slashes: true
    // }));
    parentWindow = new BrowserWindow({title: 'Parent'});
    // con la propiedad parent especificamos cual es el padre
    // con modal que no pueda clickear en el padre si no se ha cerrado el hijo
    // con show que no se puede y luego podemos manejarlo con la promesa 'ready-to-show'
    childWindow = new BrowserWindow({parent: parentWindow, title: 'Child', modal: true,show:false});
    childWindow.loadURL('https://github.com/');
    childWindow.once('ready-to-show',()=>{
        childWindow.show();
    })
}

app.on('ready',createWindow);