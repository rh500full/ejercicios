const fs = require('fs');
const path = require('path');

let btnCreate = document.getElementById('btnCreate');
let btnRead = document.getElementById('btnRead');
let btnDelete = document.getElementById('btnDelete');
let fileName = document.getElementById('fileName');
let fileContents = document.getElementById('fileContents');

let pathName = path.join(__dirname,'files');

btnCreate.addEventListener('click',()=>{
    let file = path.join(pathName,fileName.value);
    let contents = fileContents.value;
    fs.writeFile(file,contents,function (err) {
        if(err){
            return console.log(err);
        }
        console.log('archivo creado');
        
    })
})

btnRead.addEventListener('click',()=>{
    let file = path.join(pathName,fileName.value);
    fs.readFile(file,function (err,data) {
        if(err){
            return console.log(err);
        }
        fileContents.value = data;
        console.log('archivo leído');
        
    })
})

btnDelete.addEventListener('click',()=>{
    let file = path.join(pathName,fileName.value);
    fs.unlink(file,function (err,data) {
        if(err){
            return console.log(err);
        }
        fileName.value = "";
        fileContents.value = "";
        console.log('archivo leído');
        
    })
})