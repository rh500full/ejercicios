var electron = require('electron');
var {app,BrowserWindow} = electron;
var path = require('path');
var url = require('url');

var win;
const mysql = require('mysql');
const connection = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: '',
    database: 'pruebaelectron'
});
connection.connect((err)=>{
    console.log(err);
})

exports.connection = connection;
exports.closeConnection = ()=> {
    connection.end();
}

exports.openWindow = ()=>{
    let newWin = new BrowserWindow ({width: 400, height: 200});
    newWin.loadURL(url.format({
        pathname: path.join(__dirname,'prueba.html'),
        protocol: 'file',
        slashes: true
    }))
}

app.on('ready',()=>{
    win = new BrowserWindow ({width: 800, height: 600, icon: __dirname+'/favicon.ico'});
    win.loadURL(url.format({
        pathname: path.join(__dirname,'index.html'),
        protocol: 'file',
        slashes: true
    }))
    // win.webContents.openDevTools();
})