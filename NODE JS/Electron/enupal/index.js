const remote = require('electron').remote;
const main = remote.require('./main.js');

$(function () {

    let query = 'SELECT * FROM users';
    main.connection.query(query,(err,rows,fields)=>{
        if(err){
            console.log(err);
            return;
        }
        for (let i = 0; i < rows.length; i++) {
            const element = rows[i];
            $('.stats').append(`Usuarios: <span>${element.userName}</span>`);
        }
    })
    main.closeConnection(); 

    //leer rss
    const os = require('os');
    const prettyBytes = require('pretty-bytes');

    $('.stats').append(`Número de procesadores: <span>${os.cpus().length}</span>`)
    $('.stats').append(`Memoria libre: <span>${prettyBytes(os.freemem())}</span>`)

    let ul = $('.flipster ul');
    
    $.get('https://enupal.com/blog/rss',(res)=>{
        
        const rss = $(res);
        rss.find('item').each(function () {
            const item = $(this);
            const content = item.finde('description').html().split('</a></div>')[0]+'</a></div>';

            const urlRegex = /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/g;

            const imageSource = content.match(urlRegex)[1].slice(19);
            
            const li = $('<li><img /><a target="_blank"></a></li>');
            li.find('a')
                .attr('href',item.find('link').text())
                .html('<br>'+item.find('title'));

            li.find('img').attr('src',imageSource);
            li.find('img').attr('width',400);
            li.find('img').attr('width',300);
            
            li.appendTo(ul);
            
        });

    });
    $('.flipster').flipster({
        style: 'carousel'
    });
})