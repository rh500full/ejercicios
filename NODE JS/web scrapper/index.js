var cheerio = require('cheerio');
var fs = require('fs');
var request = require('request');

var images = [];

request('https://www.taringa.net/shouts',(err,res,body)=>{
    if(!err && res.statusCode == 200){
        
        $ = cheerio.load(body);
        $('img.og-img','.shouts-list').each(function () {
            var urlImg = $(this).attr('src');
            images.push(urlImg);
        })
    }
    for (let i = 0; i < images.length; i++) {
        const element = images[i];
        request(element).pipe(fs.createWriteStream(`img/${i}.jpg`));
    }
})