module.exports = function(controller) {
    const { BotkitConversation } = require('botkit');
    const convo = new BotkitConversation('convo',controller);
    
    controller.interrupts('help', 'message', async(bot, message) => {
        // start a help dialog, then eventually resume any ongoing dialog
        // await bot.beginDialog(HELP_DIALOG);
    });
    
    controller.interrupts('quit', 'message', async(bot, message) => {
        await bot.reply(message, 'Quitting!');
    
        // cancel any active dialogs
        await bot.cancelAllDialogs();
    });

    function fillingOnboarding() {
        convo.say('Hola humano!');
        convo.ask('Cual es tu nombre?', async(response,convo,bot) => { 
            bot.say(`Joya ${response}`)
        }, {key: 'name'});
        
        // <-- probando flujo de dialogo -->

        // collect a value with conditional actions
        // convo.say('Un gusto {{vars.name}}')
        // convo.ask('Te gustan los tacos?', [
        //     {
        //             pattern: 'si',
        //             handler: async function(answer, convo, bot) {
        //                 await convo.gotoThread('likes_tacos');
        //             }
        //         },
        //         {
        //             pattern: 'no',
        //             handler: async function(answer, convo, bot) {
        //                 await convo.gotoThread('hates_life');
        //             }
        //         }
        //     ],{key: 'tacos'});
        
        // // define a 'likes_tacos' thread
        // convo.addMessage('Aguanten los tacos', 'likes_tacos');
        
        // // define a 'hates_life' thread
        // convo.addMessage('Que mal!', 'hates_life');
        // <-- fin probando flujo de dialogo -->

        // use add action to switch to a different thread, defined below...
        convo.addAction('favorite_color');

        // add a message and a prompt to a new thread called `favorite_color`
        convo.addMessage('Esta bien {{vars.name}}','favorite_color');
        convo.addQuestion('Dime, cual es tu color favorito?',async(response,convo,obot)=>{
            console.log(`Color favorito ${response}`);
        },'color','favorite_color')

        // go to a confirmation
        convo.addAction('confirmation' ,'favorite_color');
        
        // do a simple conditional branch looking for user to say "no"
        convo.addQuestion('Tu nombre es {{vars.name}} y tu color favorito es {{vars.color}}. Eso esta bien?', 
        [
            {
                pattern: 'no',
                handler: async(response, convo, bot) => {
                    // if user says no, go back to favorite color.
                    await convo.gotoThread('favorite_color');
                }
            },
            {
                default: true,
                handler: async(response, convo, bot) => {
                    // do nothing, allow convo to complete.
                }
            }
        ], 'confirm', 'confirmation');

        // handle the end of the conversation
        convo.after(async(results, bot) => {
            const name = results.name;
        });
    }

    function fillingConvo() {
        // // first, define a thread called `next_step` that we'll route to...
        // convo.addMessage({
        //     text: 'This is the next step...',
        // },'next_step');
        
        
        // // send a message, and tell botkit to immediately go to the next_step thread
        // convo.addMessage({
        //     text: 'Anyways, moving on...',
        //     action: 'next_step',
        // });

        convo.addMessage({
            text: 'Tu dijiste que si!'
        },'yes_thread');
        convo.addMessage('Dijiste que no','no_thread');
        convo.addMessage({
            text: 'Lo siento no te entendi',
            action: 'default'
        },'bad_response');

        convo.addQuestion('Te gusta el queso?',
        [
            {
                pattern: 'si',
                handler: async (response,convo,bot)=>{
                    await convo.gotoThread('yes_thread');
                }
            },
            {
                pattern: 'no',
                handler: async (response,convo,bot)=>{
                    await convo.gotoThread('no_thread');
                }
            },
            {
                default: true,
                handler: async (response,convo,bot)=>{
                    await convo.gotoThread('bad_response');
                }
            }
        ],'likes_cheese','default')

    }

    function fillingReplies() {
        convo.ask('Que te gustaria ver?', [], 'reply_title');
        convo.say({
            text: 'Aqui esta tu boton dinamico:',
            quick_replies: async(template, vars) => { return [{title: vars.reply_title, payload: vars.reply_title }]}
        });
    }
     
    function parentDialog() {
        let parentId = 'parent';
        let childId = 'child';
        
        let parent = new BotkitConversation(parentId,controller);
        let child = new BotkitConversation(childId,controller);

        parent.say('Tengo un par de preguntas');
        parent.addChildDialog(childId,'answers');

        child.ask('Como te llamas?',[],'pregunta1');
        child.ask('Como es tu apellido?',[],'pregunta2');

        controller.addDialog(parent);
        controller.addDialog(child);
        controller.afterDialog(parent,async(bot,results)=>{
            
            let respuesta1 = results.answers.pregunta1;
            let respuesta2 = results.answers.pregunta2;
            await bot.say(`Gracias ${respuesta1} ${respuesta2}`)
        })
        
    }

    controller.addDialog(convo)
    controller.hears('hola','message',async (bot,message)=>{
        fillingOnboarding();
        await bot.beginDialog('convo');
    })

    controller.hears('menu','message', async(bot, message) => { 
        
        await bot.reply(message, {
            text: 'Here is a menu!',
            quick_replies: [
                {
                    title: "Main",
                    payload: "main-menu",
                },
                {
                    title: "Help",
                    payload: "help"
                }
            ]
        });
    });

    controller.hears(new RegExp(/^reboot (.*?)$/i), 'message', async(bot, message) => {

        // message.matches is the result of message.text.match(regexp) so in this case the parameter is in message.matches[1]
        let param = message.matches[1];
        await bot.reply(message, `I will reboot ${ param }`);
    
    });
    controller.hears('.*','message', async(bot, message) => {
        await bot.reply(message, 'I heard: ' + message.text);
    });
      
    // controller.middleware.receive.use(function(bot, message, next) {

    //     // log it
    //     console.log('RECEIVED: ', message.text);
    
    //     // modify the message
    //     message.logged = true;
    
    //     // continue processing the message
    //     next();
    
    // });

    // controller.middleware.send.use(function(bot, message, next) {

    //     // log it
    //     console.log('SENT: ', message.text);
    
    //     // modify the message
    //     message.logged = true;
    
    //     // continue processing the message
    //     next();
    
    // });

    // // use a function to match a condition in the message
    // controller.hears(async(message) => message.text && message.text.toLowerCase() === 'foo', ['message'], async (bot, message) => {
    //     await bot.reply(message, 'I heard "foo" via a function test');
    // });

    // // use a regular expression to match the text of the message
    // controller.hears(new RegExp(/^\d+$/), ['message','direct_message'], async function(bot, message) {
    //     await bot.reply(message,{ text: 'I heard a number using a regular expression.' });
    // });

    // // match any one of set of mixed patterns like a string, a regular expression
    // controller.hears(['allcaps', new RegExp(/^[A-Z\s]+$/)], ['message','direct_message'], async function(bot, message) {
    //     await bot.reply(message,{ text: 'I HEARD ALL CAPS!' });
    // });

}