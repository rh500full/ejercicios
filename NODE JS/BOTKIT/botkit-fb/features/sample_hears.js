/**
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License.
 */
module.exports = function(controller) {

    // const { BotkitConversation } = require('botkit');
    // const convo = new BotkitConversation('convo',controller);

    // convo.say('Hola humano!');
    // convo.ask('Cual es tu nombre?', async(response,convo,bot) => { 
        // bot.say(`Joya ${response}`)
    // }, {key: 'name'});

    // controller.addDialog(convo);
    controller.hears('hola','message',async(bot,message)=>{
        // await bot.reply(message,'hola humano');
        await bot.say({
            text: "elige",
            quick_replies: [
                {
                    title: "prueba",
                    payload: "prueba"
                },
                {
                    title:"otra cosa",
                    payload: "otra cosa"
                }
            ]
        })
    })

    controller.hears('prueba','message',async(bot,message)=>{
        await bot.say(`Elegiste prueba`)
    })

    controller.hears("otra cosa",'message',async(bot,message)=>{
        await bot.say(`Elegiste otra cosa`)
    })
    
    // use a function to match a condition in the message
    controller.hears(async(message) => message.text && message.text.toLowerCase() === 'foo', ['message'], async (bot, message) => {
        await bot.reply(message, 'I heard "foo" via a function test');
    });

    // use a regular expression to match the text of the message
    controller.hears(new RegExp(/^\d+$/), ['message','direct_message'], async function(bot, message) {
        await bot.reply(message,{ text: 'I heard a number using a regular expression.' });
    });

    // match any one of set of mixed patterns like a string, a regular expression
    controller.hears(['allcaps', new RegExp(/^[A-Z\s]+$/)], ['message','direct_message'], async function(bot, message) {
        await bot.reply(message,{ text: 'I HEARD ALL CAPS!' });
    });

}