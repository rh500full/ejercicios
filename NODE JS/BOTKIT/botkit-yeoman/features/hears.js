const { BotkitConversation } = require("botkit");
const Messages = require('../models/Messages');

module.exports = function(controller) {
    let convo = new BotkitConversation('convo',controller);

    let conversation = '',
    first_question = 'Hola buenas tardes, ¿tuviste una entrevista con nosotros?',
    second_question = '¿Que edad tienes?';

    
    controller.middleware.send.use(function(bot, message, next) {
        // log it
        conversation+= 'question: '+message.text+'|';
    
        // modify the message
        message.logged = true;
    
        // continue processing the message
        next();
    
    });


    convo.addAction('interview_ask');
    convo.addQuestion(
        {
            text: first_question,
            quick_replies:
            [
                {
                    title: 'Hace mas de un año, y no avance luego de la entrevista.',
                    payload: 'Hace mas de un año, y no avance luego de la entrevista.'
                },
                {
                    title: 'Hace más de un año y luego participe de una capacitacion',
                    payload: 'Hace más de un año y luego participe de una capacitacion'
                },
                {
                    title: 'Hace menos de un año, y no avance luego de la entrevista',
                    payload: 'Hace menos de un año, y no avance luego de la entrevista'
                },
                {
                    title: 'Hace menos de un año y luego participe de una capacitación',
                    payload: 'Hace menos de un año y luego participe de una capacitación'
                },
                {
                    title: 'Nunca participe en entrevista previamente',
                    payload: 'Nunca participe en entrevista previamente'
                }    
            ]
        },
        async(response,convo,bot)=>{
            conversation+= 'response:'+response+'|';
        },
        'entrevista',
        'interview_ask');
    
    
    convo.addAction('age_ask','interview_ask');
    convo.addQuestion(
        {
            text: second_question,
            quick_replies:
            [
                {
                    title: 'Entre 18 - 20',
                    payload: 'Entre 18 - 20'
                },
                {
                    title: 'Entre 20 - 30',
                    payload: 'Entre 20 - 30'
                },
                {
                    title: 'Entre 30 - 40',
                    payload: 'Entre 30 - 40'
                },
                {
                    title: 'Entre 40 - 50',
                    payload: 'Entre 40 - 50'
                },
                {
                    title: 'Más de 50',
                    payload: 'Más de 50'
                },
            ]
        },
        async(response,convo,bot)=>{
            conversation+= 'response:'+response+'|';
        },
        'edad',
        'age_ask');
    
    convo.after(async(results,bot)=>{
        
        let data = {
            entrevista: results.entrevista,
            edad: results.edad
        }
        const sms = new Messages({
            userId: 1,
            type: 'conversation',
            text: conversation,
            data
        });
       await sms.save().then(()=>{
           console.log('mensaje guardado');
       }) 
    })

    controller.addDialog(convo);

    controller.hears('hola','message',async(bot,message)=>{
        await bot.beginDialog('convo');
    })

    controller.on('hello',async(bot,message)=>{
        const saludo = `Hola`;
        if(global.url != ""){
            var name = global.url.split('=')[1];
            saludo+= ' '+name;
        }
        
        await bot.reply(message,saludo)
    })

    controller.on('welcome_back',async(bot,message)=>{
        if(global.url != ""){
            var name = global.url.split('=')[1];
        }
        
        await bot.reply(message,`Hola de nuevo ${name}`)
    })

}