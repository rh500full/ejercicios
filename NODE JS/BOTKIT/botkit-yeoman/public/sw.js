var CACHE_NAME = 'sw-ex';
var CACHE_VERSION = 1;

var filesToCache = [
  '/',
  '/index.html',
  '/css/styles.css',
  '/client.js'
];

self.oninstall = function(event) {
  event.waitUntil(
    caches.open(CACHE_NAME + '-v' + CACHE_VERSION).then(function(cache) {
      return cache.addAll(filesToCache);
    })
  );
};

self.onactivate = function(event) {
  var currentCacheName = CACHE_NAME + '-v' + CACHE_VERSION;
  caches.keys().then(function(cacheNames) {
    return Promise.all(
      cacheNames.map(function(cacheName) {
        if (cacheName.indexOf(CACHE_NAME) == -1) {
          return;
        }

        if (cacheName != currentCacheName) {
          return caches.delete(cacheName);
        }
      })
    );
  });
};

// self.onfetch = function(event) {
//   var request = event.request;
//   event.respondWith(
//     caches.match(request).then(function(response) {
//       if (response) {
//         return response;
//       }

//       return fetch(request).then(function(response) {
//         var responseToCache = response.clone();
//         caches.open(CACHE_NAME + '-v' + CACHE_VERSION).then(
//           function(cache) {
//             cache.put(request, responseToCache).catch(function(err) {
//               console.warn(request.url + ': ' + err.message);
//             });
//           });
//         return response;
//       });
//     })
//   );
// };


self.addEventListener('fetch', function(event) {
  // event.respondWith(fetch(event.request));
  // or simply don't call event.respondWith, which
  // will result in default browser behaviour
});

self.addEventListener('push',e=>{
  var data = e.data.json();
  console.log('Push recibida');
  self.registration.showNotification(data.title,{
      body: 'Notificado por botkit',
      icon: 'https://pbs.twimg.com/profile_images/803642201653858305/IAW1DBPw_400x400.png'
  })
})

// Broadcast via postMessage.
function sendMessage(message) {
  self.clients.matchAll().then(function(clients) {
    clients.map(function(client) {
      return client.postMessage(message);
    })
  });
}


