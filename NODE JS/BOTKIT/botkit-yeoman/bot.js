//  __   __  ___        ___
// |__) /  \  |  |__/ |  |  
// |__) \__/  |  |  \ |  |  

// This is the main file for the botkit yeoman bot.

// Import Botkit's core features
const { Botkit } = require('botkit');
const { BotkitCMSHelper } = require('botkit-plugin-cms');

// Import a platform-specific adapter for web.
const { WebAdapter } = require('botbuilder-adapter-web');

//importar librerias de npm
const mongoose = require('mongoose');

// Load process.env values from .env file
require('dotenv').config();

let storage = null;
global.url = "";

const adapter = new WebAdapter({});

function myExpressMiddleware(req,res,next) {
    if(req.url.includes('userName')){
        global.url = req.url;
    }
    next();
}

const controller = new Botkit({
    webhook_uri: '/api/messages',

    adapter: adapter,

    storage
});

if (process.env.cms_uri) {
    controller.usePlugin(new BotkitCMSHelper({
        uri: process.env.cms_uri,
        token: process.env.cms_token,
    }));
}

controller.webserver.use(myExpressMiddleware);

// Once the bot has booted up its internal services, you can use them to do stuff.
controller.ready(() => {
    // load traditional developer-created local custom feature modules
    controller.loadModules(__dirname + '/features');

    mongoose.connect('mongodb://localhost:27017/botkit',{ useNewUrlParser: true , useUnifiedTopology: true})

    /* catch-all that uses the CMS to trigger dialogs */
    if (controller.plugins.cms) {
        controller.on('message,direct_message', async (bot, message) => {
            let results = false;
            results = await controller.plugins.cms.testTrigger(bot, message);

            if (results !== false) {
                // do not continue middleware!
                return false;
            }
        });
    }

});





