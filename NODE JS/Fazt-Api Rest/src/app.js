const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const express = require('express');
const app = express();
const userRoutes = require('./routes/users');

mongoose.connect('mongodb://localhost/restApi',{useNewUrlParser: true})
.then(db=> console.log('base de datos conectada'))
.catch(err=> console.log(err))

//settings
app.set('port',process.env.PORT || 3000);

//middleware
app.use(bodyParser.json());

//routes
app.use('/users',userRoutes);

//static failes

app.listen(app.get('port'),()=>{
    console.log('aplicacion escuchando en puerto',app.get('port'));
})