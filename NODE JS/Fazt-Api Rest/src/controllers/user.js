const User = require('../models/user');
const Car = require('../models/car');

module.exports = {
    index : async (req,res,next)=>{
            const users = await User.find({});
            // throw new Error('error malo')
            res.json(users);
    },

    newUser : async (req,res,next)=>{
        var newUser = new User(req.body);
        const user = await newUser.save();
        res.json(user);
    },

    getUser : async (req,res,next)=>{
        var {userId} = req.params;
        var user = await User.findById(userId);
        res.json(user);
    },
    
    replaceUser : async (req,res,next)=>{
        var {userId} = req.params;
        var newUser = req.body;
        var oldUser = await User.findByIdAndUpdate(userId,newUser);
        res.status(200).json({success: true})
    },

    updateUser : async (req,res,next)=>{
        var {userId} = req.params;
        var newUser = req.body;
        var oldUser = await User.findByIdAndUpdate(userId,newUser);
        res.status(200).json({success: true})
    },

    deleteUser : async (req,res,next)=>{
        var {userId} = req.params;
        await User.findByIdAndDelete(userId);
        res.status(200).json({success: true})
    },

    getCars : async (req,res,next)=>{
        var {userId} = req.params;
        var user = await User.findById(userId).populate('cars');
        res.status(200).json(user);
    },

    newUserCar : async (req,res,next)=>{
        var {userId} = req.params;
        var newCar = new Car(req.body);
        var user = await User.findById(userId);
        newCar.seller = user;
        await newCar.save();
        user.cars.push(newCar);
        await user.save();
        res.status(201).json(newCar)
    }
}