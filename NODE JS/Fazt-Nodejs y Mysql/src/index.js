var app = require('./config/server');

var news = require('./app/routes/news')(app);

app.listen(app.get('port'),()=>{
    console.log('servidor en puerto:'+app.get('port'));
});