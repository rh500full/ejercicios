var dbConnection = require('../../config/dbConnection');
module.exports = app =>{
    var connection = dbConnection();

    app.get('/',(req,res)=>{
        connection.query('select * from news',(error,resultado)=>{
            res.render('news/news',{
                news: resultado
            })
        })
    }); 

    app.post('/news',(req,res)=>{
        var {title,text} = req.body;
        connection.query('insert into news SET?',{
            title,
            text
        },(err,result) => {
            res.redirect('/');
        });
    });
}