var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var app = express();

//settings
app.set('port',process.env.PORT || 90);
app.set('view engine','ejs');
app.set('views',path.join(__dirname,'../app/views'));
//middlewares

app.use(bodyParser.urlencoded({extended:false}))

module.exports = app;