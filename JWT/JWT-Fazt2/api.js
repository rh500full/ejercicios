const express = require('express'); 
const apiRoutes = express.Router();
const User = require('./app/models/user');
const jwt = require('jsonwebtoken');

apiRoutes.get('/',(req,res)=>{
    res.json({
        message: 'bienvenido'
    })
})
apiRoutes.post('/authenticate',(req,res)=>{
    User.findOne({
        name: req.body.name
    }, (err,user)=>{
        if(err) throw err;

        if(!user){
            res.json({
                success: true,
                message: 'no existe el usuario'
            })
        }
        else{
            if(user.password != req.body.password){
                res.json({
                    success: false,
                    message: 'la contraseña es incorrecta'
                })
            }
            else{
                const token = jwt.sign({user},req.app.get('superSecret'));
                res.json({
                    success: true,
                    message: 'tu token',
                    token
                })
            }
        }
    })
})

apiRoutes.use((req,res,next)=>{
    const token = req.body.token || req.query.token || req.headers['x-access-token'];
    if(token){
        jwt.verify(token,req.app.get('superSecret'),(err,data)=>{
            if(err){
                res.json({
                    success: false,
                    message: 'autenticacion fallida'
                })
            }
            else{
                req.decoded = data;
                next();
            }
        })
    }
    else{
        res.status(403).send({
            success: false,
            message: 'no existe el token'
        })
    }
})

apiRoutes.get('/users',(req,res)=>{
    User.find({},(err,users)=>{
        if(err) throw err;
        res.json({
            users
        })
    })
})


module.exports= apiRoutes;