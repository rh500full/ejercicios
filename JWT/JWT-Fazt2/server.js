const express = require('express');
const app = express();

const bodyParser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');

const config = require('./config');
const User = require('./app/models/user');
const apiRoutes = require('./api');

// settings
const port = process.env.PORT || 3000;
mongoose.connect(config.database,{
    useNewUrlParser : true
});
mongoose.Promise = global.Promise;

app.set('superSecret', config.secret);


// middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(morgan('dev'));

//routes
app.get('/',(req,res)=>{
    res.send('Hola, la api esta en : localhost:3000/api');
}) 

app.get('/setup',(req,res)=>{
    var user = new User({
        name: 'pepito',
        password: 'pepito|123',
        admin: true
    })

    user.save(err => {
        if(err) throw err;
        console.log('Usuario guardado bien');
        res.json({
            success: true
        })
    })
})

app.use('/api',apiRoutes);


app.listen(3000,()=>{
    console.log('escuchando en el puerto 3000');
})