const express = require('express'); 
const jwt = require('jsonwebtoken');
var app = express();

app.get('/',(req,res)=>{
    res.json({
        text: 'funciona'
    })
})

app.post('/api/login',(req,res)=>{
    const user  = {id:3};
    const token = jwt.sign({user},'secret_token');
    res.json({
        token
    })
})

app.get('/api/protected',ensureToken ,(req,res)=>{
    jwt.verify(req.token,'secret_token',(err,data)=>{
        if(err){
            res.sendStatus(403);
        }
        else{
            res.json({
                text:'protegido',
                data
            })
        }
    })
})

function ensureToken(req,res,next) {
    const bearerHeader = req.headers['authorization'];
    if(typeof bearerHeader !== 'undefined'){
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    }
    else{
        res.sendStatus(403)
    }
}

app.listen(3000, ()=>{
    console.log("escuchando en puerto 3000");
})