import React from 'react';
import './App.css';
//props
// function Helloworld(props) {
//   return(
//     <div id="hello">{props.text}</div>
//   )
// }

class Helloworld extends React.Component{

  //estado
  state = {
    show: true
  }

  toggleShow = ()=>{
    this.setState({show: !this.state.show})
  }

  render(){
    if(this.state.show){
      return (
        <div id="hello">{this.props.text}
          <button onClick={this.toggleShow}>Ocultar</button>
        </div>
      )
    }
    else{
      return (
        <div>
          <h1>no hay elementos</h1>
          <button onClick={this.toggleShow}>Mostrar</button>
        </div>
      )
    }
  }
}

// const App = ()=> <div>Este es mi componente: <Helloworld/></div>

function App() {
  return (
    <div>Este es mi componente: 
      <Helloworld text = "hola mundo"/> 
      <Helloworld text = "hola"/>
    </div>
  );
}

// class App extends React.Component(){
//   render(){
//     return <div>Este es mi componente: <Helloworld/></div>
//   }
// }

export default App;
