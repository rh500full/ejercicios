import React,{Component} from 'react';
import { BrowserRouter as Router,Route,Link } from "react-router-dom";

import './App.css';

import tasks from './sample/tasks.json';

// componentes
import TaskForm from './components/TaskForm';
import Tasks from './components/Tasks';
import Posts from './components/Posts'

// import { throwStatement } from '@babel/types';

class App extends Component{

  state = {
    tasks: tasks
  }

  addTask = (title,description)=>{
    var newTask  ={
      title,    
      description,
      id: this.state.tasks.length
    }
    this.setState({
      tasks:  [...this.state.tasks,newTask]
    })
  }

  deleteTask = (id)=>{
    const newTasks = this.state.tasks.filter(task => task.id !== id)
    this.setState({tasks : newTasks})
  }

  checkDone = (id)=>{
    const newTasks = this.state.tasks.map(task=>{
      if(task.id === id){
        task.done = !task.done;
      }
      return task;
    })
    this.setState({tasks: newTasks})
  }

  render(){
    return (
      <Router>
        <Link to="/">Home</Link>
        <Link to="/posts">Posts</Link>

        <Route exact path="/" render={()=>{
          return <div>
            <TaskForm addTask = {this.addTask}/>
            <Tasks tasks = {this.state.tasks} deleteTask = {this.deleteTask} checkDone={this.checkDone}/>
          </div>
        }}>

        </Route>
        <Route path="/posts" component={Posts}>

        </Route>
      </Router>
    )
  }
}

export default App;
