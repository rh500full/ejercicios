import React,{ Component } from "react";

export default class TaskForm extends Component{
    
    state = {
        title: '',
        descripcion: ''
    }

    onSubmit = e=>{
        this.props.addTask(this.state.title,this.state.descripcion)
        e.preventDefault()
    }

    onChange = e =>{
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    render(){
        
        return (
            <form onSubmit={this.onSubmit}>
                <input onChange={this.onChange} value={this.state.title} name="title" type="text" placeholder="Escribe a tarea"/>
                <br/>
                <br/>
                <textarea onChange={this.onChange} value={this.state.descripcion} name="descripcion" placeholder="Escribe una descripcion">
                </textarea>
                <input type="submit" />
            </form>
        )
    }
}
