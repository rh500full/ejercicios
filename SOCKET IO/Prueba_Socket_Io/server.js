var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

app.use(express.static('public'));

// handle incoming connections from clients
io.sockets.on('connection', function(socket) {
    // once a client has connected, we expect to get a ping from them saying what room they want to join
    socket.on('room', function(room) {
        socket.join(room);
        io.sockets.in('123').emit('message', 'what is going on, party people?');
        io.sockets.in('1231').emit('message', 'anyone in this room yet?');
    });
});

// now, it's easy to send a message to just the clients in a given room
room = "abc123";

// this message will NOT go to the client defined above
  


server.listen(90,()=>{
    console.log("corriendo en 90");
    
})