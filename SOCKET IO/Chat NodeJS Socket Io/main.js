var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var messages = [];

app.use(express.static('public'));

io.on('connection',(socket)=>{
    
    socket.emit('messages',messages);
    socket.on('response-message',(data)=>{
        messages.push(data);
        io.sockets.emit('messages',messages);
    })
})

app.get('/',(req,res)=>{
    res.status(200).send("hola")
})


server.listen(90,function () {
    console.log("corriendo en el puerto 90");
});