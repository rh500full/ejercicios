var socket = io.connect('http://localhost:90/');

socket.on('messages',(data)=>{
    render(data);
})

function render(data) {
    var html = data.map(function (elem,index) {
        return(`<div>
                    <strong>${elem.autor}</strong>
                    <em>${elem.message}</em>
                </div>`)
    }).join(" ");

    document.getElementById('messages').innerHTML = html;
}

function enviarMensaje() {
    var payload = {
        autor: document.getElementById('autor').value,
        message: document.getElementById('message').value
    }
    socket.emit('response-message',payload);
    return false;
}