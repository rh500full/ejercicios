var app = require('express')()
var http = require('http').Server(app)
var io = require('socket.io')(http)

app.get('/',function (req,res) {
    res.sendFile(__dirname+'/index.html')
})

var nsp = io.of('/mio')
var numRoom = 0;
nsp.on('connection',function (socket) {
    if(nsp.adapter.rooms["room"+numRoom]&& nsp.adapter.rooms["room"+numRoom].length>1) numRoom++;
    socket.join("room"+numRoom)
    nsp.in("room"+numRoom).emit('connectRoom',"estas en la habitacion nº"+numRoom)
})

http.listen(3000,function () {
    console.log("corriendo en 3000");
})