var app = require('express')()
var http = require('http').Server(app)
var io = require('socket.io')(http)

app.get('/',function (req,res) {
    res.sendFile(__dirname+'/index.html')
})

var nsp = io.of('/mio')
nsp.on('connection',function (socket) {
    console.log('se conectaron');
    nsp.emit('message','hola a todos')
})

http.listen(3000,function () {
    console.log("corriendo en 3000");
})