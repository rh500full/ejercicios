var app = require('express')()
var server = require('http').Server(app)
var io = require('socket.io')(server)

app.get('/',function (req,res) {
    res.sendFile(__dirname+'/index.html')
})

server.listen(3000,function () {
    console.log("corriendo en 3000");
})
io.on('connection',function (socket) {
    socket.on('chat-message',function (data,fn) {
        socket.broadcast.emit('chat-response',data)
        fn('¡woot')
    })
})